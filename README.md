# Generic pipeline for building images

Pipeline can be divided into two main subbranches: build and test.
The pipeline uses open-source solutions like kaniko and trivy. 

## Trviy
Trivy is an open-source vulnerability scanner designed for container images, providing fast and comprehensive security assessments to help identify and fix potential security issues.

1. Trivy scans the tarball for vulnerabilities 
## Kaniko

Kaniko is a tool that enables container image building inside a container or Kubernetes cluster, making it suitable for environments where traditional Docker-based image building may not be feasible or allowed.

1. Use kaniko to build the image. ([kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html))
2. Push it to the repository with crane. ([crane](https://github.com/google/go-containerregistry/tree/main))

## Pushing to any branch except develop, release, main
This pipeline is designed to build the tarball and check it, without pushing it to the container registry. 

Rules define when the pipeline should run:
1. Never at commit to develop or main
2. Never at merge request to develop, main or release 
3. Otherwise on commits to other branches if the changes are done to all the files except of README.md
4. The artifacts stay only 10min and are removed. 

Similar rules are defined for the tests.

![Logic behind the pipeline](/images/main.png)


## Commit/merge to main and creating a new tag in main
This pipeline will push the image to the container registry.

1. For the branch develop the pipeline will run on merge request and commit to the branch.


![Logic behind the pipeline](/images/default_buils.png)